import { Box, AppBar, Toolbar, IconButton, Typography, Button, Stack } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import { Link } from 'react-router-dom'

const NavBar = () => {

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Stack direction="row" spacing={3} flexGrow={1}>
            <Typography variant="h6">
              <Link to="/" style={{ textDecoration: "none", color: "white" }}>
                Home
              </Link>
            </Typography>
            <Typography variant="h6">
              <Link to="/posts" style={{ textDecoration: "none", color: "white" }}>
                Posts
              </Link>
            </Typography>
            <Typography variant="h6">
              <Link to="/about" style={{ textDecoration: "none", color: "white" }}>
                About
              </Link>
            </Typography>
          </Stack>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  )

}

export default NavBar;