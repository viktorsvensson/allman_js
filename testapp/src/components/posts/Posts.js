import { useEffect, useState } from "react";
import AddPostForm from "./AddPostForm";
import Post from "./Post";
import { Grid } from '@mui/material'
import Counter from './../Counter'
import { useSearchParams } from "react-router-dom";


const Posts = () => {

    const [queryParam, setQueryParam] = useSearchParams({ title: "" })

    const [posts, setPosts] = useState([])

    useEffect(() => {

        fetch('http://localhost:5000/api/post')
            .then(res => res.json())
            .then(data => setPosts(data))

    }, [])

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <Counter />
                </Grid>
                <Grid item xs={4}>
                    <input
                        placeholder="FIlter by title..."
                        value={queryParam.title}
                        onChange={(e) => setQueryParam({ title: e.target.value })}
                    />
                    {posts
                        .filter(post => (!queryParam.has("title") || post.title.startsWith(queryParam.get("title"))))
                        .map((post, index) => <Post key={post.id} post={post} setPosts={setPosts} />)}
                </Grid>
                <Grid item xs={4}>
                    <AddPostForm setPosts={setPosts} />
                </Grid>
            </Grid>
        </div>
    )
}

export default Posts;