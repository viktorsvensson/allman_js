import { useState } from "react";
import { Button, TextField, Stack } from "@mui/material"

const AddPostForm = ({ setPosts }) => {



    //const [title, setTitle] = useState("");
    //const [body, setBody] = useState("");
    const [input, setInput] = useState({ title: "", body: "" })

    const handleChange = (e) => {
        setInput(prev => ({ ...prev, [e.target.name]: e.target.value }))
    }

    const handleSave = async () => {
        //setPosts(prev => prev.concat(input))

        await fetch("http://localhost:5000/api/post", {
            method: "POST",
            body: JSON.stringify(input),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        let response = await fetch("http://localhost:5000/api/post");
        let body = await response.json()

        setPosts(body)
    }

    return (
        <div>
            <h3>Add post form</h3>
            <Stack spacing={2} alignItems="center">
                <TextField
                    id="standard-title"
                    label="Title"
                    variant="standard"
                    value={input.title}
                    name="title"
                    onChange={handleChange}
                />
                <TextField
                    id="standard-body"
                    label="Body"
                    variant="standard"
                    value={input.body}
                    name="body"
                    onChange={handleChange}
                />
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleSave}>
                    Add
                </Button>
            </Stack>
        </div>
    )
}

export default AddPostForm;