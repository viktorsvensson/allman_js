import { Card, Stack } from '@mui/material';

const Post = ({ post, setPosts }) => {

    const handleDelete = async () => {

        await fetch("http://localhost:5000/api/post/" + post.id, {
            method: 'DELETE'
        })

        fetch("http://localhost:5000/api/post")
            .then(res => res.json())
            .then(data => setPosts(data))

    }

    return (
        <Stack margin={2}>
            <Card variant='outlined'>
                <h2>{post.title}</h2>
                <p>{post.body}</p>
                <button onClick={handleDelete}>remove</button>
            </Card>
        </Stack>
    )
}

export default Post;