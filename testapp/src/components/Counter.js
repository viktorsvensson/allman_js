import { useEffect, useState } from 'react';

const Counter = () => {

    //let count = 5;
    const [count, setCount] = useState(0)

    useEffect(() => {
        setTimeout(() => {
            console.log(count)
        }, 2000)
    }, [count])

    const handleIncrease = () => {
        setCount(prev => prev + 1)
    }

    return (
        <div>
            <p>The count right now is: {count}</p>
            <button onClick={handleIncrease}>Increase</button>
        </div>
    )
}

export default Counter;