import './App.css';
import NavBar from './components/NavBar';
import Posts from './components/posts/Posts';
import { Routes, Route } from 'react-router-dom'
import Home from './components/Home';
import About from './components/About';
import SinglePostView from './Views/SinglePostView';
import { ThemeProvider, createTheme } from '@mui/material/styles'
import { formHelperTextClasses } from '@mui/material';
import "@fontsource/praise"

const theme = createTheme({
  palette: {
    primary: {
      main: '#000000'
    }
  },
  typography: {
    fontFamily: [
      'Praise'
    ]
  }


})

function App() {

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <NavBar />
        <Routes>
          <Route path="/posts/:id" element={<SinglePostView />} />
          <Route path="/posts" element={<Posts />} />
          <Route path="/about" element={<About />} />
          <Route path="/" element={<Home />} />
        </Routes>
      </div>
    </ThemeProvider>
  );
}

export default App;
