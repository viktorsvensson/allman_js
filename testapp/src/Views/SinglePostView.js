import { Button } from "@mui/material"
import { useNavigate, useParams } from "react-router-dom"


const SinglePostView = () => {

    const { id } = useParams()

    const navigate = useNavigate()

    const fakeFetch = (id) => ({
        title: "Placeholder title",
        body: "Placeholder body",
        id: id
    })

    const post = fakeFetch(id)

    return (
        <div>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
            <p>{post.id}</p>
            <Button onClick={() => navigate("/posts")}> View all posts</Button>
        </div>
    )


}

export default SinglePostView;