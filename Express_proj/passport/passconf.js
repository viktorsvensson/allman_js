import { Strategy } from "passport-local"

const passconf = (passport, loadUserBy) => {

    const verify = async (username, password, done) => {

        // Callback for getting user from database (or other source)
        const user = await loadUserBy(username)

        // Check if user exist
        if (!user) {
            return done(null, false)
        }

        console.log("pass", password, "userpass", user.password)
        // Check if password matches
        if (password == user.password) {
            console.log("true")
            return done(null, user)
        } else {
            done(null, false)
        }

    }

    passport.use(new Strategy({ usernameField: 'username' }, verify))

    // Serialize 
    passport.serializeUser((user, done) => done(null, user.username))

    // Deserialize
    passport.deserializeUser(async (username, done) => done(null, await loadUserBy(username)))

    return passport;

}

export default passconf;