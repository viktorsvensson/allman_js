import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const seed = async () => {

    await prisma.users.deleteMany();
    await prisma.todos.deleteMany();

    await prisma.users.create({
        data: {
            username: "Alice",
            password: "1234"
        }
    })

    await prisma.users.create({
        data: {
            username: "Gunnar",
            password: "1234"
        }
    })

    await prisma.todos.create({
        data: {
            id: 1,
            title: "Alice första todo",
            user_id: 1
        }
    })


}

seed();

export default seed;