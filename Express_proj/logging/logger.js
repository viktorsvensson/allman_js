// Simple request logger middleware
const logger = async (req, res, next) => {
    console.log(
        req.method,
        req.originalUrl,
        req.user?.username,
        new Date().toLocaleString()
    )

    next()
}

export default logger;