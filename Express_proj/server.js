import express from 'express';
import 'dotenv/config';
import userRoute from './routes/userRoutes.js'
import logger from './logging/logger.js';
import passport from 'passport';
import passconf from './passport/passconf.js';
import { PrismaClient } from '@prisma/client';
import session from 'express-session';
import postRoute from './routes/postRoutes.js'
import cors from 'cors';

const prisma = new PrismaClient();


// Skapar express-server
const server = express();
const PORT = process.env.PORT || 5000;

// Parsar data som JSON
server.use(express.json())

// Configure cors policy
const corsConfig = {
    origin: "http://localhost:3000"
}

server.use(cors(corsConfig))

// Configure express-session
server.use(session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,

}))

// Configure passport
passconf(
    passport,
    username => prisma.users.findUnique({
        where: {
            username: username
        }
    })
)

server.use(passport.initialize())
server.use(passport.session())

// Login route, authenticates the user provided username and password
server.post("/login", passport.authenticate('local', {}), (req, res) => res.sendStatus(200))

// Check if authenticated middleware
const isAuthenticated = (req, res, next) => {
    req.isAuthenticated ? next() : res.sendStatus(401)
}

// Uses our logger middleware
server.use(logger)

// Route for testing the logger middleware
server.get("/log", (req, res) => res.sendStatus(200))

server.use("/api/post", postRoute)

// Routes for /api/user
server.use("/api/user", isAuthenticated, userRoute)

// Mappar GET-anrop till viss address
server.get("/", ((req, res) => res.json({ message: "Hello World!" })))

// Lyssna på angiven port
server.listen(PORT, () => console.log(`Server started on ${PORT}`))