import express from "express";
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

const route = express.Router();

// GET all users
route.get("/", async (req, res) => {

    const users = await prisma.users.findMany({})
    // .then(users => res.json(users))

    res.json(users)
})

// GET by id
route.get("/:id", async (req, res) => {

    const user = await prisma.users.findUnique({
        where: {
            id: parseInt(req.params.id)
        },
        include: {
            todos: true
        }
    })

    res.json(user)

})

// POST new user
route.post("/", async (req, res) => {

    const user = await prisma.users.create({
        data: {
            fname: req.body.fname,
            lname: req.body.lname
        }
    })

    res.json(user)

})

// DELETE
route.delete("/:id", async (req, res) => {

    await prisma.users.delete({
        where: {
            id: parseInt(req.params.id)
        }
    })

    res.sendStatus(204)
})

// Patch
route.patch("/:id", async (req, res) => {
    const user = await prisma.users.update({
        where: {
            id: parseInt(req.params.id)
        },
        data: {
            username: req.body.username
        }
    })

    res.json(user)
})

export default route;