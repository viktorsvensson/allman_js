import express from 'express';

const route = express.Router();

let posts = [
    { id: 1, title: "Min första fetchade post", body: "lite brödtext" },
    { id: 2, title: "Min andra fetchade post", body: "lite brödtext" },
    { id: 3, title: "Min tredje fetchade post", body: "lite brödtext" },
]

let idCounter = 3;

route.get("/", (req, res) => {
    res.json(posts)
})

route.post("/", (req, res) => {
    idCounter++
    const newPost = { ...req.body, id: idCounter }
    posts = posts.concat(newPost);
    res.json(newPost)
})

route.delete("/:id", (req, res) => {
    const id = req.params.id;

    console.log(id)

    posts = posts.filter(post => !(post.id == id))
    res.sendStatus(201)
})

export default route;